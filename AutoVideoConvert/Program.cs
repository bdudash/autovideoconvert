﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Diagnostics;

namespace AutoVideoConvert
{
    class Program
    {
        const string FFMPEGCMD = "ffmpeg.exe";
        
        string ffMpegLocation = ".";

        static void PrintSyntax(string command)
        {
            Console.WriteLine(command + "<source> [<destination>]");
            Console.WriteLine();
            Console.WriteLine("<source> = The source directory to read video files from");
            Console.WriteLine("<destination> = [optional] The destination directory to save video files to. Defaults to <source>.");
            Console.WriteLine();
            Console.WriteLine("This tool will convert all video files of supported extensions in the sources folder into mp4 files and save to destination.");
        }

        static bool ConvertFile(string exe, string source, string dest)
        {
            Console.WriteLine("-----------------------------------------------------------------------------------------");
            Console.WriteLine("Begin conversion on \"" + source + "\" to \"" + dest + "\"");
            Console.WriteLine("-----------------------------------------------------------------------------------------");
            var p = new Process();
            var psi = new ProcessStartInfo(exe,"-i "+source + " " + dest);
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            p.StartInfo = psi;
            p.Start();
            p.WaitForExit();
            StreamReader sr = p.StandardOutput;
            Console.WriteLine(sr.ReadToEnd());
            p.Close();
            return true;
        }

        static string[] monthNames = {"EvilMagic","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

        static void Main(string[] args)
        {
            string fullCommandExe = System.Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + FFMPEGCMD;
            bool bFound = File.Exists(fullCommandExe);
            bool bSourceDirExists = args.Length > 0 ? Directory.Exists(args[0]) : false;
            bool bDestDirExists = args.Length > 1 ? Directory.Exists(args[1]) : false;
            if (!bFound || !bSourceDirExists)
            {
                PrintSyntax("BINGO");
                return;
            }

            var sourceDir = args[0];
            var destDir = bDestDirExists?args[1]:args[0];
            var sourceDirInfo = new DirectoryInfo(sourceDir);
            var destDirInfo = new DirectoryInfo(destDir);

            foreach(var fileInfo in sourceDirInfo.EnumerateFiles("*.mts",SearchOption.TopDirectoryOnly))
            {
                int m = fileInfo.LastWriteTime.Month;
                int d = fileInfo.LastWriteTime.Day;
                int y = fileInfo.LastWriteTime.Year;
                int h = fileInfo.LastWriteTime.Hour;
                int min = fileInfo.LastWriteTime.Minute;

                string month = m < 10 ? "0" + m.ToString() : m.ToString();
                string day = d < 10 ? "0" + d.ToString() : d.ToString();
                string year = y.ToString();
                string hour = h < 10 ? "0" + h.ToString() : h.ToString();
                string minute = min < 10 ? "0" + min.ToString() : min.ToString();

                string newFileName = month + day + "_" + year + "_" + hour + "_" + minute + "_" + fileInfo.Name.Substring(0, fileInfo.Name.Length - 3) + "mp4";

                var destSubDir = year + System.IO.Path.DirectorySeparatorChar + month + "_" + monthNames[m] + System.IO.Path.DirectorySeparatorChar;
                var destSubDirInfo = new DirectoryInfo(destDirInfo.FullName + System.IO.Path.DirectorySeparatorChar + destSubDir);
                if (!destSubDirInfo.Exists) destSubDirInfo.Create();
                var destFileInfo = new FileInfo(destSubDirInfo.FullName + newFileName);
                if (!destFileInfo.Exists)   // skip file if it has already been made to allow continuation on an aborted process!
                {
                    ConvertFile(fullCommandExe, fileInfo.FullName, destFileInfo.FullName);
                    destFileInfo.LastWriteTime = fileInfo.LastWriteTime;
                }

            }
        }
    }
}
